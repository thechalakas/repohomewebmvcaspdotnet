# repohomewebmvcaspdotnet

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to repos related to asp.net MVC things.

**here are the repos**

I have written some code related dot net mvc. I got some stuff here that i use for work and also training. 

---

1. [https://bitbucket.org/thechalakas/demo_asp_dot_net_1]

2. [https://bitbucket.org/thechalakas/webhookpractice1]

3. [https://bitbucket.org/thechalakas/workingwithdata]

This project is about how to manually connect with a local DB.

4. [https://bitbucket.org/thechalakas/manualdbsample]
5. [https://bitbucket.org/thechalakas/consoledbusage]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 